package com.c0d3in3.lecture8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init() // initializing activity
    }

    private fun init(){
        val email = emailEditText.text  // creating variable of emailEditText for easy access
        val password = passwordEditText.text // creating variable of passwordEditText for easy access

        logInButton.setOnClickListener{
            if(email!!.isEmpty()) return@setOnClickListener Toast.makeText(this, "E-mail field must be filled!", Toast.LENGTH_SHORT).show() //checking email field
            if(password!!.isEmpty()) return@setOnClickListener Toast.makeText(this, "Password field must be filled!", Toast.LENGTH_SHORT).show() //checking password field
            if(isEmailValid(email.toString())){ // checking email for email format
                if(password.length in 7..31){ // checking password for length  | range=[6-32]
                    Toast.makeText(this, "You have authorized successfully!", Toast.LENGTH_LONG).show()
                }
                else{
                    Toast.makeText(this, "Password must be in length 6-32!", Toast.LENGTH_LONG).show()
                }
            }
            else Toast.makeText(this, "Invalid E-mail format! E-mail must include '@' and '.' !", Toast.LENGTH_LONG).show()
        }

        forgotPasswordTextView.setOnClickListener {
            Toast.makeText(this, "Forgot password is under development!", Toast.LENGTH_LONG).show()
        }

        signUpTextView.setOnClickListener {
            Toast.makeText(this, "Sign up is under development!", Toast.LENGTH_LONG).show()
        }
    }

//    checking email format with android utility
    //checks email with regex given below
    //private val emailRegex = compile(
    //        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
    //                "\\@" +
    //                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
    //                "(" +
    //                "\\." +
    //                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
    //                ")+"
    //)
    private fun isEmailValid(email: String): Boolean {
      return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    // simple and bad email validation method XD but works for 5/10
    //returning true if valid and email contains @ and .
    //else returning false
   // private fun isEmailValid(email: String) = email.contains("@") && email.contains(".")
}
